import random

from string import ascii_letters

import pytest


@pytest.fixture
def random_model_name():
    """
    Fixture that produces a random Model name
    :return: a random Model name
    """
    return "ModelName" + "".join([random.choice(ascii_letters) for _ in range(10)])


@pytest.fixture
def random_table_name():
    """
    Fixture that produces a random table name
    :return: a random table name
    """
    return "table_name_" + "".join([random.choice(ascii_letters) for _ in range(10)])
