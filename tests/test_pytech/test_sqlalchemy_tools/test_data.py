from dataclasses import dataclass
from datetime import datetime

from sqlalchemy.schema import Column
from sqlalchemy.types import Integer

from pytech.sqlalchemy_tools.services import build_sqlalchemy_model


class NotDataclass:
    """
    Simple empty class
    """
    pass


@dataclass
class SimpleDataclass:
    """
    Simple dataclass with some annotations
    """
    my_custom_datetime: datetime
    my_custom_float: float
    my_custom_integer: int
    my_custom_string: str


@dataclass
class ExtendedDataclass(SimpleDataclass):
    """
    Extended dataclass with more annotations
    """
    another_datetime: datetime
    another_float: float
    another_integer: int
    another_string: str


@dataclass
class DataclassWithWrongAnnotations:
    """
    Simple dataclass with not allowed annotation
    """
    custom_field: any


SimpleModel = build_sqlalchemy_model(
    from_dataclass=SimpleDataclass,
    table_name="simple_dataclass",
    model_name="SimpleModel",
    pk_fields=["my_custom_integer"]
)

SimpleModelAdditionalFields = build_sqlalchemy_model(
    from_dataclass=SimpleDataclass,
    table_name="simple_dataclass_with_additional_fields",
    model_name="SimpleModelAdditionalFields",
    additional_fields={
        "additional_integer_pk": Column(Integer, primary_key=True)
    }
)
