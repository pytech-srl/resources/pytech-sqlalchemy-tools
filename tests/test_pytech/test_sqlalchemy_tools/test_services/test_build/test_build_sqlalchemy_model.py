import pytest

from sqlalchemy.exc import ArgumentError
from sqlalchemy.schema import Column
from sqlalchemy.types import Integer, String

from pytech.sqlalchemy_tools.models import BaseSqlModel
from pytech.sqlalchemy_tools.services import build_sqlalchemy_model

from tests.test_pytech.test_sqlalchemy_tools.test_data import (
    DataclassWithWrongAnnotations,
    NotDataclass,
    SimpleDataclass,
)


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, "string", {}, [], tuple(), int, str, NotDataclass]
)
def test__raise_type_error_if_not_a_dataclass(wrong_data_type):
    with pytest.raises(TypeError):
        build_sqlalchemy_model(
            from_dataclass=wrong_data_type,
            table_name="name",
            model_name="Name"
        )


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, {}, [], tuple(), int, str, NotDataclass]
)
def test__raise_type_error_if_table_name_not_a_string(
        wrong_data_type,
        random_model_name
):
    with pytest.raises(TypeError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=wrong_data_type,
            model_name=random_model_name
        )


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, {}, [], tuple(), int, str, NotDataclass]
)
def test__raise_type_error_if_model_name_not_a_string(wrong_data_type):
    with pytest.raises(TypeError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name="",
            model_name=wrong_data_type
        )


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, [], tuple(), int, str, NotDataclass]
)
def test__raise_type_error_if_additional_fields_is_not_a_list(
        wrong_data_type,
        random_model_name,
        random_table_name,
):
    with pytest.raises(TypeError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            additional_fields=wrong_data_type,
        )


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, {}, tuple(), int, str, NotDataclass]
)
def test__raise_type_error_if_pk_fields_is_not_a_list(
        wrong_data_type,
        random_model_name,
        random_table_name,
):
    with pytest.raises(TypeError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            pk_fields=wrong_data_type,
        )


def test__raise_value_error_if_dataclass_has_wrong_annotations(
        random_model_name,
        random_table_name
):
    with pytest.raises(ValueError):
        build_sqlalchemy_model(
            from_dataclass=DataclassWithWrongAnnotations,
            table_name=random_table_name,
            model_name=random_model_name
        )


@pytest.mark.parametrize(
    "wrong_table_name", ["A NAME"]
)
def test__raise_value_error_if_table_name_is_not_valid(
        wrong_table_name,
        random_model_name
):
    with pytest.raises(ValueError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=wrong_table_name,
            model_name=random_model_name
        )


@pytest.mark.parametrize(
    "wrong_model_name", ["A NAME", "132", "1Name", ".Name"]
)
def test__raise_value_error_if_model_name_is_not_valid(
        wrong_model_name,
        random_table_name
):
    with pytest.raises(ValueError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=wrong_model_name
        )


@pytest.mark.parametrize(
    "wrong_primary_key", ["A NAME", "132", "1Name", ".Name"]
)
def test__raise_value_error_if_pk_fields_deos_not_contain_fields(
        wrong_primary_key,
        random_model_name,
        random_table_name,
):
    with pytest.raises(ValueError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            pk_fields=[wrong_primary_key],
        )


@pytest.mark.parametrize(
    "wrong_primary_key", ["A NAME", "132", "1Name", ".Name"]
)
def test__raise_value_error_if_additional_fields_deos_not_contain_valid_field_names(
        wrong_primary_key,
        random_model_name,
        random_table_name,
):
    with pytest.raises(ValueError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            additional_fields={wrong_primary_key: Column(Integer)},
        )


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, {}, tuple(), int, str, NotDataclass]
)
def test__raise_value_error_if_additional_fields_deos_not_contain_valid_field_values(
        random_model_name,
        random_table_name,
        wrong_data_type,
):
    with pytest.raises(ValueError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            additional_fields={"a_new_field": wrong_data_type},
        )


def test__raise_argument_error_if_no_primary_key_is_specified(
        random_model_name,
        random_table_name,
):
    with pytest.raises(ArgumentError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
        )


def test__raise_argument_error_if_additional_argument_and_no_primary_key_is_specified(
        random_model_name,
        random_table_name,
):

    with pytest.raises(ArgumentError):
        build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            additional_fields={
                "additional_field_integer": Column(Integer),
            },
        )


def test__the_model_is_built_with_existing_field_as_pk(
        random_model_name,
        random_table_name
):
    custom_model = build_sqlalchemy_model(
        from_dataclass=SimpleDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        pk_fields=["my_custom_integer"]
    )

    assert issubclass(custom_model, BaseSqlModel)


def test__the_model_is_built_with_additional_fields(
        random_model_name,
        random_table_name
):
    custom_model = build_sqlalchemy_model(
        from_dataclass=SimpleDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        additional_fields={
            "additional_field_integer": Column(Integer, primary_key=True),
            "additional_field_string": Column(String),
        },
    )

    assert issubclass(custom_model, BaseSqlModel)
