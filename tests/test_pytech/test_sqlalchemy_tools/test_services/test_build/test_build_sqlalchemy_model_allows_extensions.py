import pytest

from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.schema import Column
from sqlalchemy.types import Integer, String

from pytech.sqlalchemy_tools.models import BaseSqlModel
from pytech.sqlalchemy_tools.services import build_sqlalchemy_model

from tests.test_pytech.test_sqlalchemy_tools.test_data import (
    ExtendedDataclass,
    SimpleDataclass,
)


def test__raises_error_if_added_fields(
        random_model_name,
        random_table_name
):
    _ = build_sqlalchemy_model(
        from_dataclass=SimpleDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        pk_fields=["my_custom_integer"],
    )

    with pytest.raises(InvalidRequestError):
        _ = build_sqlalchemy_model(
            from_dataclass=ExtendedDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            pk_fields=["my_custom_integer"],
        )

    with pytest.raises(InvalidRequestError):
        _ = build_sqlalchemy_model(
            from_dataclass=SimpleDataclass,
            table_name=random_table_name,
            model_name=random_model_name,
            additional_fields={
                "additional_field_string": Column(String),
            },
            pk_fields=["my_custom_integer"],
        )


def test__the_model_can_be_extended_adding_dataclass_fields(
        random_model_name,
        random_table_name
):
    custom_model = build_sqlalchemy_model(
        from_dataclass=SimpleDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        pk_fields=["my_custom_integer"],
    )

    assert issubclass(custom_model, BaseSqlModel)

    custom_model = build_sqlalchemy_model(
        from_dataclass=ExtendedDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        pk_fields=["my_custom_integer"],
        extensible_model=True,
    )

    assert issubclass(custom_model, BaseSqlModel)


def test__the_model_can_be_extended_adding_additional_fields(
        random_model_name,
        random_table_name
):
    custom_model = build_sqlalchemy_model(
        from_dataclass=SimpleDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        pk_fields=["my_custom_integer"],
    )

    assert issubclass(custom_model, BaseSqlModel)

    # Add a field but keep the Primary Key

    custom_model = build_sqlalchemy_model(
        from_dataclass=SimpleDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        additional_fields={
            "additional_field_string": Column(String),
        },
        pk_fields=["my_custom_integer"],
        extensible_model=True,
    )

    assert issubclass(custom_model, BaseSqlModel)

    # Add fields and change the Primary Key

    custom_model = build_sqlalchemy_model(
        from_dataclass=SimpleDataclass,
        table_name=random_table_name,
        model_name=random_model_name,
        additional_fields={
            "additional_field_integer": Column(Integer, primary_key=True),
            "additional_field_string": Column(String),
        },
        extensible_model=True,
    )

    assert issubclass(custom_model, BaseSqlModel)
