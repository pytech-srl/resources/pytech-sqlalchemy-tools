from datetime import datetime

import pytest

from sqlalchemy.exc import ArgumentError
from sqlalchemy.schema import CreateTable
from sqlalchemy.sql import insert

from pytech.sqlalchemy_tools.services.retrieve import retrieve_all_data
from tests.test_pytech.test_sqlalchemy_tools.test_data import (
    NotDataclass,
    SimpleModel,
    SimpleModelAdditionalFields,
)


@pytest.fixture(autouse=True)
def create_simple_models(mocked_session):
    """
    Auto-use fixture that creates the tables for the tests
    It also adds values inside the tables

    :param mocked_session: a mocked session that writes in memory
    """
    mocked_session.execute(
        CreateTable(SimpleModel.__table__)
    )
    mocked_session.execute(
        CreateTable(SimpleModelAdditionalFields.__table__)
    )

    with mocked_session:
        mocked_session.execute(
            insert(SimpleModel).values(
                my_custom_datetime=datetime.now(),
                my_custom_float=1.0,
                my_custom_integer=1,
                my_custom_string="my_string",
            )
        )
        mocked_session.execute(
            insert(SimpleModelAdditionalFields).values(
                my_custom_datetime=datetime.now(),
                my_custom_float=1.0,
                my_custom_integer=1,
                my_custom_string="my_string",
                additional_integer_pk=1
            )
        )
        mocked_session.commit()

    yield


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, "string", {}, [], tuple(), int, str, NotDataclass]
)
def test__raise_type_error_if_not_a_session(wrong_data_type):
    with pytest.raises(TypeError):
        retrieve_all_data(
            session=wrong_data_type,
            model=SimpleModel,
        )


@pytest.mark.parametrize(
    "wrong_data_type", [0, 1.2, {}, [], tuple(), int, str, NotDataclass]
)
def test__raise_type_error_if_table_name_not_a_model(wrong_data_type, mocked_session):
    with pytest.raises(TypeError):
        retrieve_all_data(
            session=mocked_session,
            model=wrong_data_type,
        )


@pytest.mark.parametrize(
    "wrong_data_type", [1.2, "hello", int, str, NotDataclass]
)
def test__raise_argument_error_if_where_clause_not_a_column_expression_argument(
        wrong_data_type,
        mocked_session
):
    with pytest.raises(ArgumentError):
        retrieve_all_data(
            session=mocked_session,
            model=SimpleModel,
            where_clause=wrong_data_type
        )


@pytest.mark.parametrize(
    "model", [SimpleModel, SimpleModelAdditionalFields]
)
def test__returns_a_list_of_values(model, mocked_session):
    retrieved_values = retrieve_all_data(
        session=mocked_session,
        model=model,
    )

    assert isinstance(retrieved_values, list)
    assert len(retrieved_values)


@pytest.mark.parametrize(
    "where_clause", [
        SimpleModel.__table__.c.my_custom_integer == 1,
        SimpleModel.__table__.c.my_custom_integer >= 1,
        SimpleModel.__table__.c.my_custom_datetime >= datetime(year=2023, month=1, day=1),  # noqa:E501
    ]
)
def test__returns_a_list_of_values_with_a_condition(mocked_session, where_clause):
    retrieved_values = retrieve_all_data(
        session=mocked_session,
        model=SimpleModel,
        where_clause=where_clause
    )

    assert isinstance(retrieved_values, list)
    assert len(retrieved_values)
