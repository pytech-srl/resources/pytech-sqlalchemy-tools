# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
import sys
from datetime import datetime
from zoneinfo import ZoneInfo
from pathlib import Path


BASE_PATH = Path(__file__).resolve().parents[2] / "src"
CURRENT_YEAR = datetime.now().year

sys.path.insert(0, str(BASE_PATH))
sys.path.insert(0, str(BASE_PATH / "pytech"))
sys.path.insert(0, str(BASE_PATH / "pytech" / "sqlalchemy_tools"))
sys.path.insert(0, str(BASE_PATH / "pytech" / "services"))


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "PyTech SQL Alchemy Tools Project"
copyright = f"{CURRENT_YEAR}, PyTech srl"
author = "PyTech srl"
release = "2.0.1"
today_date = (
    datetime.now()
    .astimezone(ZoneInfo("Europe/Rome"))
    .strftime("%Y-%m-%d, %H:%M:%S")
)

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.duration",
    "sphinx.ext.autosectionlabel",
]

templates_path = ["_templates"]
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"
html_static_path = ["_static"]

# to get the date in .rst file
rst_epilog = (
    f".. |DocumentationVersion| replace:: Last Version: {release} "
    f"released at {today_date}"
)
